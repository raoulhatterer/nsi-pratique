def eratosthene(n):
    tableau = [True] * n
    tableau[0] = ...
    tableau[1] = ...
    for i in range(..., n):
        if tableau[i] == ...:
            for multiple in range(2*i, n, i):
                tableau[...] = ...
    return [i for i in range(n) if ...]


# Tests
assert eratosthene(2) == []
assert eratosthene(3) == [2]
assert eratosthene(7) == [2, 3, 5]
assert eratosthene(40) == [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37]

