---
author: BNS2022-22.2, puis Nicolas Revéret
title: Crible d'Ératosthène
tags:
  - boucle
---

# Crible d'Ératosthène

Un nombre premier est un nombre entier naturel qui admet exactement deux diviseurs distincts entiers et positifs : $1$ et lui-même.

Par exemple :

* $0$ n'est pas premier car il est divisible par $1$, $2$, $3$, ...
* $1$ n'est pas premier car il n'est divisible **que** par $1$,
* $2$ et $3$ sont premiers,
* $4$ n'est pas premier car il est divisible par $1$, $2$ et $4$,
* $5$ est premier.

Le crible d'Ératosthène permet de déterminer les nombres premiers plus petit qu'un certain nombre $n$ fixé. La démarche est la suivante :

* on écrit tous les nombres plus petit que $n$
* on raye $0$ et $1$ qui ne sont pas premiers
* le prochain nombre non rayé est un nombre premier
* on raye tous les multiples de ce nombre
* on recommence : le prochain nombre non rayé est premier, on raye ses multiples *etc*...

![Crible d'Ératosthène](images/crible.gif){ width="50%" }

Avec Python, si l'on cherche les nombres premiers strictement inférieurs à `n` :
  
* on considère un tableau de booléens `tableau`, initialement tous égaux à `True`, sauf `tableau[0]` et `tableau[1]` qui valent `False` ($0$ et $1$ ne sont pas premiers)

* On parcourt ce tableau de gauche à droite. Pour chaque indice `i` :

    * si `tableau[i]` vaut `True` : le nombre $i$ est premier. On l'ajoute à `premiers` et on donne la valeur `False` à toutes les cellules de `tableau` dont l'indice est un multiple de `i`, à partir de `2*i` (c'est-à-dire `2*i`, `3*i`, ...)
    * si `tableau[i]` vaut `False` : le nombre $i$ n'est pas premier. On n'effectue aucun changement sur le tableau.

* on renvoie une liste contenant les indices des cellules de `tableau` valant `True`


!!! tip "Astuce"

    L'instruction Python `range(2*i, n, i)` permet de parcourir tous les multiples de `i` strictement inférieurs à `n`

Compléter la fonction `eratosthene` :

* prenant en paramètre un entier `n` strictement positif
* renvoyant une liste contenant tous les nombres premiers strictement plus petits que `n`.


!!! example "Exemples" 

    ```pycon
    >>> eratosthene(2)
    []
    >>> eratosthene(3)
    [2]
    >>> eratosthene(7)
    [2, 3, 5]
    >>> eratosthene(40)
    [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37]
    ```

{{ IDE('exo') }}
