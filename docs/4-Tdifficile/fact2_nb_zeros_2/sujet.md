---
author: Franck Chambon
title: Nombre de zéros de n! (2)
tags:
  - maths+
  - boucle
---
# Nombre de zéros de n factoriel ; n grand

On rappelle que, pour $n$ un entier naturel, la factorielle de $n$ se note $n!$ et se définit comme le produit des entiers de $1$ à $n$.

- $0! = 1$, comme un produit vide.
- $1! = 1$
- $2! = 1×2 = 2$
- $3! = 1×2×3 = 6$
- $11! = 1×2×3×4×5×6×7×8×9×10×11 = 39916800$
- $42! = 1405006117752879898543142606244511569936384000000000$

On constate que

- $3!$ se termine par aucun zéro.
- $11!$ se termine par 2 zéros.
- $42!$ se termine par 9 zéros.

Construire une **fonction**, tel que `nb_zeros_factorielle(n)` renvoie le nombre de zéros dans l'écriture décimale de $n!$, pour $n$ entier inférieur à $10^{18}$.

!!! example "Exemples"

    ```pycon
    >>> nb_zeros_factorielle(3)
    0
    >>> nb_zeros_factorielle(11)
    2
    >>> nb_zeros_factorielle(42)
    9
    ```

{{ IDE('exo') }}

??? tip "Indice 1"
    - Il n'est pas possible de construire un tableau de tous les résultats.
    - Il faut penser décomposition en facteurs premiers de $n!$.
    - Une divisibilité par $10$ signifie la présence de $2$ et $5$ dans la décomposition.
    - Le nombre de zéros d'un nombre est le minimum de l'exposant de 2 et de celui de 5 dans la décomposition en facteurs premiers du nombre.
    - Pour $n!$, le nombre de zéros est l'exposant de 5 qui est plus petit que l'exposant de 2.

??? tip "Indice 2"
    - Combien y a-t-il de multiples de 5 de $1$ à $n$ ? Chacun apporte un zéro.
    - Combien y a-t-il de multiples de 25 de $1$ à $n$ ? Chacun apporte un zéro supplémentaire.
    - Combien y a-t-il de multiples de 125 de $1$ à $n$ ? Chacun apporte un zéro supplémentaire.
    - Quand faut-il arrêter cette liste ? Et comment la générer ?

??? tip "Indice 3"
    - On utilisera une variable `puiss_5` qui sera une puissance de 5.
    - La puissance suivante s'obtient avec `puiss_5 *= 5`.
    - Il y a `n // k` multiples de $k$ de $1$ à $n$, par exemple :
        - Il y a `42 // 5 = 8` multiples de 5 de $1$ à $42$ : $5$, $10$, $15$, $20$, $25$, $30$, $35$ et $40$.
        - Il y a `42 // 25 = 1` multiple de 25 de $1$ à $42$ : $25$.
        - Il y a `42 // 125 = 0` multiple de 125 de $1$ à $42$.
    - Il y a $8+1+0=9$ zéros à la fin de $42!$
