# Exercices pratiques en NSI

Des exercices à résoudre en ligne.

- des exercices faciles à coder entièrement,
- des codes à trous à compléter, dans les entrées prépondérantes,
- d'autres exercices plus difficiles.

Les auteurs sont cités dans le fichier source `sujet.md` de chaque exercice.

Les enseignants qui souhaitent contribuer peuvent rejoindre le forum des enseignants de NSI.


## Comment est construit ce site :hammer_and_wrench:

Ce site est rédigé en [Markdown](https://markdown.org), construit de façon statique avec [MkDocs](https://mkdocs.org) et le thème [Material](https://squidfunk.github.io/mkdocs-material/). Cette construction se fait à partir des fichiers hébergés sur la plateforme de développement [GitLab](https://about.gitlab.com) (lien vers le dépôt en bas de page).

Les logiciels qui servent à construire et publier ce site sont des [logiciels libres](https://www.april.org/articles/divers/intro_ll.html) et respectent
les 4 libertés :

1. Utiliser le logiciel pour tous les usages
2. Étudier le code source du logiciel
3. Distribuer le logiciel de façon libre
4. Améliorer le logiciel et distribuer les améliorations.



Le site fonctionne donc sans tracker de suivi ni cookies (aucune mesure d'audience). Le code JavaScript exécuté sert pour la console de développement intégré sur les pages et nous l'hébergeons également. Seul MathJax (utilisé pour le rendu des formules mathématiques) est servi depuis un cdn (cdn.jsdeliver.net)
